package com.example.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.DynamicInsert;



@Entity
@DynamicInsert
@Table(name = "employee")
public class Employee implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "address")
	private String address;
	
	@Pattern(regexp = "^[0-9]*$", message = "Character must be numeric")
	@Column(name = "phone")
	private String phoneNumber;
		
	@Column(name = "emergencycontact", nullable = true)
	private String emergencyContact;
	
	@Column(name = "wage")
	private int wage;
	
	@Column(name = "employedat")
	private Date employedAt;
	
	@Column(name = "resignation", columnDefinition = "varchar(255) default 'NOT RESIGNED'")
	private String resign;
	
	@Column(name = "createdat", columnDefinition = "TIMESTAMP default NOW()")
	private Date createdAt;
	
	@Column(name = "createdby", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String createdBy;
	
	@Column(name = "updatedat", columnDefinition = "TIMESTAMP default NOW()")
	private Date updatedAt;
	
	@Column(name = "updatedby", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public int getWage() {
		return wage;
	}

	public void setWage(int wage) {
		this.wage = wage;
	}

	public Date getEmployedAt() {
		return employedAt;
	}

	public void setEmployedAt(Date employedAt) {
		this.employedAt = employedAt;
	}

	public String getResign() {
		return resign;
	}

	public void setResign(String resign) {
		this.resign = resign;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	

}
