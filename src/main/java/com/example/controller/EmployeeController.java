package com.example.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.exception.ResourceNotFoundException;
import com.example.model.Employee;

import com.example.repository.EmployeeRepository;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeRepository er;
	
	@GetMapping("/employee")
	public Page<Employee> getQuestion(Pageable pageable) {
		return er.findAll(pageable);
		
	}
	
	@PostMapping("/employee")
	public Employee createEmployee(@Valid @RequestBody Employee employee) {
		return er.save(employee);
		
	}
	
	@PutMapping("/employee/{employeeId}")
	public Employee updateEmployee(@PathVariable Long employeeId, @Valid @RequestBody Employee employeeRequest) {
		return er.findById(employeeId).map(employee->{
			employee.setName(employeeRequest.getName());
			employee.setAddress(employeeRequest.getAddress());
			employee.setPhoneNumber(employeeRequest.getPhoneNumber());
			employee.setEmergencyContact(employeeRequest.getEmergencyContact());
			return er.save(employee);
		}).orElseThrow(()->new ResourceNotFoundException("Employee Id not found with id " + employeeId));
				
	}
	
	@DeleteMapping("employee/{employeeId}")
	public ResponseEntity<?> deleteEmployee(@PathVariable Long employeeId){
		return er.findById(employeeId).map(employee->{
			er.delete(employee);
			return ResponseEntity.ok().build();
		}).orElseThrow(()-> new ResourceNotFoundException("Employee Id not found with id " + employeeId));
				
	}
	
}
